const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update/:id',(req,res)=>{
     if(req.body.productName && req.body.price)
	 {
    let found=0     
	for(let i=0;i<products.length;i++)
	{
      if(products[i].id==req.params.id)
      { 
          products[i].price=req.body.price;
          products[i].productName=req.body.productName;
          found=1
          res.status(200).send(products[i]);
      }
	}
    if(found==0)
    {
         res.status(200).send('The product does not exist!');
    }
     }else
	 {
         res.status(500).send("Error!");
     }
});

app.delete('/delete',(req,res)=>
{
    if(req.body.payload)
	{
        let found=0;
        for(let i=0;i<products.length;i++)
	{
      if(products[i].productName==req.body.payload)
      { 
        products.splice(i,1);
        res.status(200).send("The product has been deleted!");
        found=1;
      }
	}
      if(found==0)
	  {
           res.status(200).send('The product could not be found!');
      } 
    }else
    
    res.status(500).send('Error');
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});